# README #

This README would normally document whatever steps are necessary to get your application up and running.

This approach will help to have a attachment with all details of the respective record and can able to send an Email with this Attachment to customers by selecting required records using checkbox. 

### How do I get set up? ###

Deploy as it is.

### Who do I talk to? ###

sandeep.polishetty@absyz.com



### Steps to Use ###
Consider a record of Invoice with all required Lineitems. 
Step1:Click on ‘Generate Invoice’ button
![1.JPG](https://bitbucket.org/repo/8zXMGnd/images/3511003010-1.JPG)
Step2: You will be redirected to a page as shown below. This page will helps us to ensure the Invoice details are correct. 

If all details are correct. Click on ‘Generate PDF’ button.
![2.JPG](https://bitbucket.org/repo/8zXMGnd/images/3650262158-2.JPG)

An attachment will be created to Invoice record as shown below

![3.JPG](https://bitbucket.org/repo/8zXMGnd/images/2432493871-3.JPG)

Step3: And will be redirected to the page as shown below 

This page will show all the contatcs records, you can select the required contacts to whom you want to send mail by clicking on their respective record checkbox.

Those checked records Emails will be added to ‘To’ field as shown below.
![4.JPG](https://bitbucket.org/repo/8zXMGnd/images/2131693038-4.JPG)
![5.JPG](https://bitbucket.org/repo/8zXMGnd/images/4016946008-5.JPG)

 
 

You will be getting all the email address in ‘From’ picklist from the Organization-wide Email Addresses.
Please ensure that you have addresses with verified.

Step 4: Enter Subject and Message. 

Click on ‘Send’ button

This page will reloads and shows for whom all you have send Emails in the Field ‘Email send To’ as shown below
![6.JPG](https://bitbucket.org/repo/8zXMGnd/images/2143655229-6.JPG)


The Mail will be as shown below
![7.JPG](https://bitbucket.org/repo/8zXMGnd/images/1235403589-7.JPG)


And the attachment will be as shown below.
![8.JPG](https://bitbucket.org/repo/8zXMGnd/images/3526983456-8.JPG)
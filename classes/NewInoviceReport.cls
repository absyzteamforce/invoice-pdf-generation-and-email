public class NewInoviceReport {

    public ID fileID {
        get;
        set;
    }

    public ID attinvID {
        get;
        set;
    }


    public Invoice__C inv {
        get;
        set;
    }
    public List < Account > lstAccounts {
        get;
        set;
    }

    public List < Opportunity > lstOppts {
        get;
        set;
    }



    public NewInoviceReport(ApexPages.StandardController controller) {

        fileId = ApexPages.currentPage().getParameters().get('id');
        attinvID = fileId;

        //getting all required fields values from Invoice object to show on invoice page
        inv = [select id, name, Date_Generated__c, Date_Collected__c, opportunity__c, oneAttachement__c, Currency__c, isChecked__c, BPoNumber__c, SubTotal__c, Service_Tax__c, Discount_Amount__c, Other_Tax_Amount__c, TOTAL__c, (select name, Hours_Logged__c, Resources__c, Rate__c, Price__c from Line_Items__r) from invoice__c where id =: fileId];
        //getting all required fields values from Opportunity object to show on invoice page
        lstOppts = [select id, AccountId From Opportunity WHERE Id =: inv.opportunity__c];
        //getting all required fields values from Account to show on invoice
        lstAccounts = [select id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry, (select id, FirstName, LastName FROM Contacts) From Account WHERE Id =: lstOppts[0].AccountId];

    }
    public PageReference savePdf() {
        PageReference pdf = Page.NewInvoiceReport;
        Attachment attach = new Attachment();
        Blob body;
        body = pdf.getContentAsPdf();
        attach.Body = body;
        attach.Name = inv.Name + '.pdf';
        attach.ContentType = 'application/pdf';
        attach.ParentId = fileId;
        insert attach;

        pageReference redirect = page.sendemail;
        redirect.setRedirect(true);
        // pass the selected asset ID to the new page
        redirect.getParameters().put('InvoiceId', fileID);
        redirect.getParameters().put('attachmentinvoiceID', attinvID);
        return redirect;

    }


}
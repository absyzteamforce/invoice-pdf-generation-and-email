public class CheckBoxEmailAssignmentPage {
    public List < Contact > allContacts {
        get;
        set;
    }
    public List < ContactModel > modelList {
        get;
        set;
    }
    public string to {
        get;
        set;
    }

    public string emailFromAddress {
        get;
        set;
    }
    public List < string > addresses {
        get;
        set;
    }
    public String fromAdd {
        set;
        get;
    }
    public String body {
        set;
        get;
    }
    public String subject {
        get;
        set;
    }
    public Boolean disabl {
        get;
        set;
    }

    public id xid {
        get;
        set;
    }




    public CheckBoxEmailAssignmentPage() {
        xid = ApexPages.currentPage().getParameters().get('attachmentinvoiceID');
        system.debug('AAAA' + xid);


        addresses = new List < String > ();
        integer i = 0;
        allContacts = [SELECT id, FirstName, LastName, email from Contact];
        modelList = new List < ContactModel > ();
        for (Contact con: allContacts) {
            if (con.Email != null) {
                ContactModel cm = new ContactModel();
                cm.srno = ++i;
                cm.con = con;
                modelList.add(cm);
            }
        }
    }


    public List < selectoption > getOrgWideEmailAddress() {
        List < SelectOption > lstEmailIds = new List < SelectOption > ();

        // Use Organization Wide Address 
        for (OrgWideEmailAddress owa: [select id, Address from OrgWideEmailAddress]) {
            lstEmailIds.add(new SelectOption(owa.id, owa.address));
        }
        return lstEmailIds;
    }

    public void sendEmail() {
        disabl = true;
        //Fetching attachemts based on record id
        List < attachment > att = [SELECT Id, body FROM Attachment WHERE ParentId =: xid limit 1];
        system.debug('ZZZZZZ' + att);

        //Create a master list to hold the emails we'll send
        List < Messaging.SingleEmailMessage > emails = new List < Messaging.SingleEmailMessage > ();
        //Create a new Email
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        addresses = to.split(',');

        email.setOrgWideEmailAddressId(emailFromAddress);
        //getting all attachments for respective record
        List < Messaging.Emailfileattachment > fileAttachments = new List < Messaging.Emailfileattachment > ();
        for (Attachment a: [select Name, Body, BodyLength from Attachment where ParentId =: Xid limit 1]) {
            Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
            efa.setFileName(a.Name);
            efa.setBody(a.Body);
            fileAttachments.add(efa);
        }
        email.setFileAttachments(fileAttachments);



        //Set email contents


        email.setSubject(subject);
        email.setToAddresses(addresses);
        email.setPlainTextBody(body);
        emails.add(email);
        Messaging.sendEmail(emails);

    }
    //wrapper class 
    public class ContactModel {
        public Integer srno {
            get;
            set;
        }
        public Contact con {
            set;
            get;
        }
        public String text {
            get;
            set;
        }
        public ContactModel() {
            srno = 0;
            con = new Contact();
        }
    }
}